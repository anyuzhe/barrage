package top.houry.netty.barrage.listener;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import lombok.extern.slf4j.Slf4j;

/**
 * @Desc NettyServerChannelFutureListener监听事件
 * @Author houry
 * @Date 2021/4/21 15:57
 **/
@Slf4j
public class BarrageNettyProcessorListener implements ChannelFutureListener {

    private final String msgTip;

    public BarrageNettyProcessorListener(String msgTip) {
        this.msgTip = msgTip;
    }

    /**
     * 端口绑定之后发生的操作
     *
     * @param future
     * @throws Exception
     */
    @Override
    public void operationComplete(ChannelFuture future) throws Exception {
        if (future.isSuccess()) {
            log.info("NettyServerListener-operationComplete-{}-成功", msgTip);
        } else {
            log.info("NettyServerListener-operationComplete-{}-失败", msgTip);
        }
    }
}
