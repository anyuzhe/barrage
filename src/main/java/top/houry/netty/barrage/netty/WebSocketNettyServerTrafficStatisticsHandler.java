package top.houry.netty.barrage.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;
import top.houry.netty.barrage.consts.BarrageRedisKeyConst;
import top.houry.netty.barrage.utils.BarrageRedisUtils;

import java.net.InetSocketAddress;


@Slf4j
public class WebSocketNettyServerTrafficStatisticsHandler extends ChannelInboundHandlerAdapter {

    /**
     * 读取发送的消息
     *
     * @param ctx     通道上下文
     * @param barrage 信息内容
     * @throws Exception
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object barrage) throws Exception {
        InetSocketAddress ipSocket = (InetSocketAddress) ctx.channel().remoteAddress();
        String clientIp = ipSocket.getAddress().getHostAddress();
        BarrageRedisUtils.setKeyIncrementAndExpireAt(BarrageRedisKeyConst.BARRAGE_SERVER_FLOW_CONTROL_KEY + clientIp, 10);
        ctx.fireChannelRead(barrage);
    }


}
